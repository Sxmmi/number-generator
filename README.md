# Number Generator

There are four main features of this app

(NOTE: Please hit the surprise button last)

    Toast Button: Will say merry christmas in a toast message
    Count Button: Will increment the number shown by 1
    Random Button: Will show a random number, on a second fragment screen, between 0 and the number currently displayed
    Surprise button: Will redirect user, using a webservice, the text view of the following link: https://jsonplaceholder.typicode.com/posts/1
